# EDM Production Program

This is a work-in-progress self-taught learning path for EDM production. This outline covers the courses, materials, and instrument practice necessary to get started in the field of electronic music production. The primary objective of this curriculum is as follows:

* Understand how music production works with Ableton Live
* Apply music theory through piano and drumpad practice
* Supplemental reading for creativity, music theory, and mixing audio

This curriculum will also require you to create a daily journal. The purpose of this journal is to help you get in the habit of creating a daily log to keep you inspired, reflect on the material you have learned, and to keep track of the progress you have made throughout your musical learning journey.

Below is the outline of the program you will be studying throughout the next few months, with each section split into four quarters. A weekly schedule will also be included for every quarter, with the recommended hours you should spend for each task.


### Required Materials:

* [Coursera Account](https://www.coursera.org/specializations/electronic-music-production#courses)
* [Ableton Live (Any Version)](https://www.ableton.com/en/live/)
* MIDI Keyboard/Controller
* [Melodics Subscription](https://melodics.com/)
* [The Laws Of Creativity](https://www.joeycofone.com/laws-of-creativity)
* [Open Music Theory](https://viva.pressbooks.pub/openmusictheory/)
* [Daily Journal](https://baronfig.com/products/grow-daily-journal?variant=32330427465811) 


### Recommended Instruments:

* [Arturia Mini Lab 3](https://www.guitarcenter.com/Arturia/MiniLab-3-Hybrid-Keyboard-Controller-White-1500000380992.gc)
* [Nektar Impact GX61](https://www.guitarcenter.com/Nektar/Impact-GX61-MIDI-Controller-Keyboard.gc)


### Additional Reading:

* [Mixing Audio: Concepts, Practices, and Tools - 4th Edition](https://www.amazon.com/Mixing-Audio-Concepts-Practices-Tools/dp/1032219440/ref=d_pd_sbs_sccl_2_4/140-2967454-5533165?content-id=amzn1.sym.979276af-0315-48f7-920c-ae1ddfce33e2&pd_rd_i=1032219440&psc=1)

## EDM Program Outline

### 1st Quarter

| Course        | Hours      |
| :-------------| :--------: |
| [Ableton Interactive Learning](https://learningmusic.ableton.com)     | 6 (Before Starting Coursera)  |
| [Coursera: The Technology Of Music Production](https://www.coursera.org/learn/technology-of-music-production?specialization=electronic-music-production)     | 6 (After Completing The Above)    |
| [Melodics: Piano](https://melodics.com/midi-keyboard)      | 4 + Practice  |
| [Reading: The Laws Of Creativity](https://www.joeycofone.com/laws-of-creativity)      | 2.5   |

 
### Weekly Schedule
|                           Monday                          |                     Tuesday                    |                         Wednesday                         |                    Thursday                    |                           Friday                          |                       Weekend                      |
|:---------------------------------------------------------:|:----------------------------------------------:|:---------------------------------------------------------:|:----------------------------------------------:|:---------------------------------------------------------:|:--------------------------------------------------:|
| Ableton/<br>Coursera<br>2 Hours<br><br>Reading<br>30 Mins | Piano<br>2 Hours<br><br><br>Reading<br>30 Mins | Ableton/<br>Coursera<br>2 Hours<br><br>Reading<br>30 Mins | Piano<br>2 Hours<br><br><br>Reading<br>30 Mins | Ableton/<br>Coursera<br>2 Hours<br><br>Reading<br>30 Mins | Piano Practice<br><br><br><br>Ableton<br>Recording |

 
**Total Weekly Hours: 12.5**
<br><br>

### 2nd Quarter

| Course        | Hours      |
| :-------------| :--------: |
| [Coursera: Introduction To Ableton Live](https://www.coursera.org/learn/ableton-live?specialization=electronic-music-production)     | 6    |
| [Melodics: Piano](https://melodics.com/midi-keyboard)     | 3 + Practice    |
| [Melodics: DrumPads](https://melodics.com/finger-drumming)      | 1 + Practice  |
| [Reading: The Laws Of Creativity](https://www.joeycofone.com/laws-of-creativity)      | 2.5   |


### Weekly Schedule
|                       Monday                      |                       Tuesday                       |                     Wednesday                     |                       Thursday                      |                       Friday                      |                         Weekend                         |
|:-------------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------:|:-------------------------------------------------------:|
| Coursera<br>2 Hours<br><br><br>Reading<br>30 Mins | Piano/Pads<br>2 Hours<br><br><br>Reading<br>30 Mins | Coursera<br>2 Hours<br><br><br>Reading<br>30 Mins | Piano/Pads<br>2 Hours<br><br><br>Reading<br>30 Mins | Coursera<br>2 Hours<br><br><br>Reading<br>30 Mins | Piano & Pad<br>Practice<br><br><br>Ableton<br>Recording |

 
**Total Weekly Hours: 12.5**
<br><br>

### 3rd Quarter

| Course        | Hours      |
| :-------------| :--------: |
| [Coursera: Creating Sounds For Electronic Music](https://www.coursera.org/learn/music-synthesizer?specialization=electronic-music-production)     | 6    |
| [Melodics: Piano](https://melodics.com/midi-keyboard)     | 3 + Practice    |
| [Melodics: DrumPads](https://melodics.com/finger-drumming)      | 1 + Practice  |
| [Reading: Open Music Theory](https://viva.pressbooks.pub/openmusictheory/)      | 3.75   |


### Weekly Schedule
|                       Monday                      |                       Tuesday                       |                     Wednesday                     |                       Thursday                      |                       Friday                      |                         Weekend                         |
|:-------------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------:|:-------------------------------------------------------:|
| Coursera<br>2 Hours<br><br><br>Reading<br>45 Mins | Piano/Pads<br>2 Hours<br><br><br>Reading<br>45 Mins | Coursera<br>2 Hours<br><br><br>Reading<br>45 Mins | Piano/Pads<br>2 Hours<br><br><br>Reading<br>45 Mins | Coursera<br>2 Hours<br><br><br>Reading<br>45 Mins | Piano & Pad<br>Practice<br><br><br>Ableton<br>Recording |


**Total Weekly Hours: 13.75**
<br><br>

### 4th Quarter

| Course        | Hours      |
| :-------------| :--------: |
| [Coursera: Electronic Music Performance Techniques](https://www.coursera.org/learn/edi-performance-techniques?specialization=electronic-music-production)     | 6    |
| [Melodics: Piano](https://melodics.com/midi-keyboard)     | 3 + Practice    |
| [Melodics: DrumPads](https://melodics.com/finger-drumming)      | 1 + Practice  |
| [Reading: Open Music Theory](https://viva.pressbooks.pub/openmusictheory/)      | 3.75   |


### Weekly Schedule
|                       Monday                      |                       Tuesday                       |                     Wednesday                     |                       Thursday                      |                       Friday                      |                         Weekend                         |
|:-------------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------:|:-------------------------------------------------------:|
| Coursera<br>2 Hours<br><br><br>Reading<br>45 Mins | Piano/Pads<br>2 Hours<br><br><br>Reading<br>45 Mins | Coursera<br>2 Hours<br><br><br>Reading<br>45 Mins | Piano/Pads<br>2 Hours<br><br><br>Reading<br>45 Mins | Coursera<br>2 Hours<br><br><br>Reading<br>45 Mins | Piano & Pad<br>Practice<br><br><br>Ableton<br>Recording |


**Total Weekly Hours: 13.75**
<br><br>


## Conclusion

After completing this learning path, you should have a strong foundational knowledge of electronic music production, along with a good understanding of music theory that you hopefully applied through your piano and drumpad practice. The courses on Coursera can be taken for free, but you may opt-in to receive a certificate for each course (financial aid is available on their site).
